#ifndef GAMERENDERWIDGET_H
#define GAMERENDERWIDGET_H

#include <QPointer>

#include "abstractgameview.h"
#include "abstractgamemodel.h"

class GameRenderWidget : public AbstractGameView
{
    Q_OBJECT
public:
    explicit GameRenderWidget(QWidget *parent = nullptr) noexcept;

    void setGameModel(AbstractGameModel *model) noexcept;

protected:
    void paintEvent(QPaintEvent *event) override;

private:
    QImage m_frame;
    QPointer<AbstractGameModel> m_gameModel;
};

#endif // GAMERENDERWIDGET_H
