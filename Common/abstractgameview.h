#ifndef ABSTRACTGAMEVIEW_H
#define ABSTRACTGAMEVIEW_H

#include <QWidget>

#include "abstractgamemodel.h"

class AbstractGameView : public QWidget
{
public:
    explicit AbstractGameView(QWidget *parent = nullptr) noexcept : QWidget(parent) {}
    ~AbstractGameView() = default;

    virtual void setGameModel(AbstractGameModel *model) noexcept = 0;
};

#endif // ABSTRACTGAMEVIEW_H
