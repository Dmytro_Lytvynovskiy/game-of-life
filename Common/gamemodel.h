#ifndef GAMEMODEL_H
#define GAMEMODEL_H

#include <QObject>
#include <QImage>

#include "abstractgamemodel.h"

template<typename T> class QFutureWatcher;

class GameModel : public AbstractGameModel
{
public:
    explicit GameModel(QObject *parent = nullptr) noexcept;
    ~GameModel() noexcept;

    QImage frame() const noexcept override;

    void activateGame(bool activate) noexcept override;

    void generateRandomField() noexcept override;
    void clearField() noexcept override;

    bool saveGame(const QString &path) const noexcept override;
    bool loadGame(const QString &path) noexcept override;

private:
    // run single frame processing concurrently
    void calculate() noexcept;

    // NOTE: monochrome QImage used as two-dimentional bit container
    QImage m_gameField;
    QFutureWatcher<QImage> *m_watcher;
    QTimer *m_delayedStartTimer;
    int m_threadCount;
    bool m_running;
};

#endif // GAMEMODEL_H
