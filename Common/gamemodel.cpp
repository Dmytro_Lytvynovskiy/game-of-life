#include "gamemodel.h"

#include <functional>

#include <QImage>
#include <QPainter>
#include <QDebug>
#include <QtConcurrent>

static constexpr int DEFAULT_FIELD_HEIGHT { 500 }; // px
static constexpr int DEFAULT_FIELD_WIDTH { 500 }; // px
static constexpr int ITERATION_INTERVAL { 20 }; // msec

// calculation helper struct
struct LifeSlice final {
    LifeSlice() = default;
    LifeSlice(const QRect &r, const QImage &i) noexcept
        : rect(r)
        , image(i) {}

    // rectangle that the slice belongs in
    QRect rect;
    QImage image;
};

int neighborsCount(const QImage &field, int x, int y) noexcept
{
    auto count = 0;
    const auto width = field.width();
    const auto height = field.height();

    for (int column = x - 1; column < x + 2; column++) {
        int wrappedColumn = column;
        if (wrappedColumn >= width ) {
            wrappedColumn = 0;
        } else if (wrappedColumn < 0) {
            wrappedColumn = width - 1;
        }

        for (int row = y - 1; row < y + 2; ++row) {
            // skip self-counting
            if ((column == x) && (row == y)) {
                continue;
            }

            int wrappedRow = row;
            if (wrappedRow < 0) {
                wrappedRow = height - 1;
            } else if (wrappedRow >= height) {
                wrappedRow = 0;
            }

            if (field.pixelIndex(wrappedColumn, wrappedRow) == 1) {
                count ++;
            }
        }
    }

    return count;
}

// called concurrently
LifeSlice functor(LifeSlice slice) noexcept
{
    const auto rect = slice.rect;
    const auto image = slice.image;
    const auto height = rect.height();
    const auto width = rect.width();

    auto next = QImage(rect.size(), QImage::Format_Mono);
    next.fill(0);

    for (int column = 0; column < width; ++column) {
        for (int row = 0; row < height; ++row) {
            const auto x = column + rect.x();
            const auto y = row + rect.y();
            const auto isAlive = image.pixelIndex(x, y) == 1;
            const auto neighborgs = neighborsCount(image, x, y);
            if (!isAlive && neighborgs == 3) {
                 next.setPixel(column, row, 1);
            }
            if (!isAlive) {
                continue;
            }
            if (neighborgs == 2 || neighborgs == 3) {
                next.setPixel(column,row, 1);
            }
        }
    }
    slice.image = next;
    return slice;
}

// called concurrently
void reduce(QImage &next, const LifeSlice &slice) noexcept
{
    // init field
    if (next.isNull()) {
        next = QImage(QSize(DEFAULT_FIELD_WIDTH, DEFAULT_FIELD_HEIGHT), QImage::Format_Mono);
    }
    // draw slice
    QPainter painter(&next);
    painter.drawImage(slice.rect.topLeft(), slice.image);
}

GameModel::GameModel(QObject *parent) noexcept
    : AbstractGameModel(parent)
    , m_gameField(QSize(DEFAULT_FIELD_WIDTH, DEFAULT_FIELD_HEIGHT), QImage::Format_Mono)
    , m_watcher(new QFutureWatcher<QImage>(this))
    , m_delayedStartTimer(new QTimer(this))
    , m_threadCount(QThreadPool::globalInstance()->maxThreadCount())
    , m_running(false)
{
    m_delayedStartTimer->setInterval(ITERATION_INTERVAL);
    m_delayedStartTimer->setSingleShot(true);

    connect(m_delayedStartTimer, &QTimer::timeout, this, &GameModel::calculate);

    connect(m_watcher, &QFutureWatcher<QImage>::finished, [this] () {
        m_gameField = m_watcher->result();
        emit updateFrame(m_gameField);
        if (m_running) {
            // reduce high CPU usage
            m_delayedStartTimer->start();
        }
    });

    // init empty field
    clearField();
}

GameModel::~GameModel() noexcept
{
    m_watcher->cancel();
    m_watcher->waitForFinished();
}

QImage GameModel::frame() const noexcept
{
    return m_gameField;
}

void GameModel::activateGame(bool activate) noexcept
{
    m_running = activate;

    // prevent two-times running
    const auto calculationRunning = m_watcher->isRunning() || m_delayedStartTimer->isActive();
    if (activate && !calculationRunning) {
        calculate();
    }
}

void GameModel::calculate() noexcept
{
    const auto width = m_gameField.width();
    const auto height = m_gameField.height();
    const auto segmentsCount = m_threadCount;
    const auto segmentWidth = width / segmentsCount;

    // break up into slices
    QList<LifeSlice> slices;
    for (int segment = 0; segment < m_threadCount; segment++) {
        const auto shift = segment * segmentWidth;
        const QRect rect(shift, 0, segmentWidth, height);
        LifeSlice slice(rect, m_gameField);
        slices.append(slice);
    }
    const auto future = QtConcurrent::mappedReduced<QImage>(slices, functor, reduce, QtConcurrent::UnorderedReduce);
    m_watcher->setFuture(future);
}

void GameModel::generateRandomField() noexcept
{
    clearField();

    qsrand(QTime::currentTime().msec());
    for (int i = 0; i < 10000; i++) {
        const auto x = qrand() % DEFAULT_FIELD_HEIGHT;
        const auto y = qrand() % DEFAULT_FIELD_WIDTH;
        m_gameField.setPixel(x, y, 1);
    }

    emit updateFrame(m_gameField);
}

void GameModel::clearField() noexcept
{
    m_gameField.fill(0);

    emit updateFrame(m_gameField);
}

bool GameModel::saveGame(const QString &path) const noexcept
{
    if (path.isEmpty()) {
        qDebug() << "Errors occurred while saving: file path is empty.";
        return false;
    }

    if (m_gameField.isNull()) {
        qDebug() << "Errors occurred while saving: game field is empty.";
        return false;
    }

    // save field params
    QJsonObject obj;
    obj.insert("width", m_gameField.width());
    obj.insert("height", m_gameField.height());

    // save game field
    QByteArray imageArray;
    QBuffer imageBuffer(&imageArray);
    const auto bufferOpened = imageBuffer.open(QIODevice::WriteOnly);
    if (!bufferOpened) {
        qDebug() << "Errors occurred while saving: can't open image buffer.";
        return false;
    }

    const auto saved = m_gameField.save(&imageBuffer, "PNG");
    if (!saved) {
        qDebug() << "Errors occurred while saving: can't save image to buffer.";
        return false;
    }

    const QString imageStr(imageArray.toBase64());
    obj.insert("gameField", imageStr);

    QFile file(path);
    const auto fileOpened = file.open(QIODevice::WriteOnly);
    if (!fileOpened) {
        qDebug() << "Errors occurred while saving: can't open file.";
        return false;
    }

    QJsonDocument doc(obj);
    file.write(doc.toJson());

    return true;
}

bool GameModel::loadGame(const QString &path) noexcept
{
    if (path.isEmpty()) {
        qDebug() << "Errors occurred while loading: file path is empty.";
        return false;
    }

    QFile file(path);
    const auto fileOpened = file.open(QIODevice::ReadOnly);
    if (!fileOpened) {
        qDebug() << "Errors occurred while loading: can't open file.";
        return false;
    }

    const auto doc = QJsonDocument::fromJson(file.readAll());
    if (doc.isEmpty()) {
        qDebug() << "Errors occurred while loading: JSON document is empty.";
        return false;
    }

    const auto rootObj = doc.object();
    if (rootObj.isEmpty()) {
        qDebug() << "Errors occurred while loading: root JSON object is empty.";
        return false;
    }

    // load field params
    const auto jsonWidth = rootObj.value("width");
    if (jsonWidth.isNull()) {
        qDebug() << "Errors occurred while loading: width value is empty.";
        return false;
    }

    const auto jsonHeight = rootObj.value("height");
    if (jsonHeight.isNull()) {
        qDebug() << "Errors occurred while loading: height value is empty.";
        return false;
    }

    // load game field
    const auto jsonGameField = rootObj.value("gameField");
    if (jsonGameField.isNull()) {
        qDebug() << "Errors occurred while loading: game field is empty.";
        return false;
    }

    const auto gameFieldStr = jsonGameField.toString();
    auto imageArray = QByteArray::fromBase64(gameFieldStr.toUtf8());
    QBuffer imageBuffer(&imageArray);
    QImage tempImage;
    const auto loaded = tempImage.load(&imageBuffer, "PNG");
    if (!loaded) {
        qDebug() << "Errors occurred while loading: can't load image.";
        return false;
    }

    // check image
    const auto height = jsonHeight.toInt(-1);
    const auto width = jsonWidth.toInt(-1);
    const auto validSize = tempImage.height() == height &&
                           tempImage.width() == width;
    const auto validFormat = tempImage.format() == QImage::Format_Mono;
    if (tempImage.isNull() || !validSize || !validFormat) {
        qDebug() << "Errors occurred while loading: game field is invalid.";
        return false;
    }

    m_gameField = tempImage;

    emit updateFrame(m_gameField);

    return true;
}
