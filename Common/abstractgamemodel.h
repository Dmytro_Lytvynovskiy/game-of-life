#ifndef ABSTRACTGAMEMODEL_H
#define ABSTRACTGAMEMODEL_H

#include <QObject>
#include <QImage>

class AbstractGameModel : public QObject
{
    Q_OBJECT
public:
    static const QString &saveGameFormat() {
        static const QString SAVE_GAME_FORMAT = QStringLiteral("golsave");
        return SAVE_GAME_FORMAT;
    }

    explicit AbstractGameModel(QObject *parent = nullptr) : QObject(parent) {}
    ~AbstractGameModel() = default;

    virtual QImage frame() const noexcept = 0;

    virtual void activateGame(bool activate) noexcept = 0;

    virtual void generateRandomField() noexcept = 0;
    virtual void clearField() noexcept = 0;

    virtual bool saveGame(const QString &path) const noexcept = 0;
    virtual bool loadGame(const QString &path) noexcept = 0;

signals:
    void updateFrame(const QImage &frame);
};

#endif // ABSTRACTGAMEMODEL_H
