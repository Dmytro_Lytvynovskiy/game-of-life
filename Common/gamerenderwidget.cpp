#include "gamerenderwidget.h"

#include <QPainter>

GameRenderWidget::GameRenderWidget(QWidget *parent) noexcept : AbstractGameView(parent)
{
}

void GameRenderWidget::setGameModel(AbstractGameModel *model) noexcept
{
    m_gameModel = model;

    if (!m_gameModel.isNull()) {
        m_frame = m_gameModel->frame();

        connect(m_gameModel.data(), &AbstractGameModel::updateFrame, [this] (const QImage &frame) {
            m_frame = frame;
            update();
        });
    }
}

void GameRenderWidget::paintEvent(QPaintEvent *event)
{
    Q_UNUSED(event)

    QPainter p(this);

    if (!m_frame.isNull()) {
        // scale image to widget size
        p.drawImage(rect(), m_frame);
    }
}
