#include "mainwindow.h"

#include <QLayout>
#include <QPushButton>
#include <QStateMachine>
#include <QFileDialog>

#include "gamerenderwidget.h"
#include "gamemodel.h"

MainWindow::MainWindow(QWidget *parent) noexcept
    : QWidget(parent)
    , m_stateMachine(new QStateMachine(this))
    , m_onGameState(new QState)
    , m_outOfGameState(new QState)
    , m_startStopBtn(new QPushButton(this))
    , m_generateFieldBtn(new QPushButton(this))
    , m_saveGameBtn(new QPushButton(this))
    , m_loadGameBtn(new QPushButton(this))
    , m_clearFieldBtn(new QPushButton(this))
    , m_gameWidget(new GameRenderWidget(this))
    , m_gameModel(new GameModel(this))
{
    m_gameWidget->setGameModel(m_gameModel);
    m_gameModel->generateRandomField();

    initUi();
}

QSize MainWindow::sizeHint() const
{
    return { 1000, 800 };
}

void MainWindow::initUi() noexcept
{
    connect(m_startStopBtn, &QPushButton::toggled, [this] (bool checked) {
        if (checked) {
            emit gameStarted();
        } else {
            emit gameStopped();
        }

        m_gameModel->activateGame(checked);
    });

    connect(m_generateFieldBtn, &QPushButton::clicked, [this] () {
        m_gameModel->generateRandomField();
    });

    connect(m_saveGameBtn, &QPushButton::clicked, [this] () {
        const auto pattern = QStringLiteral("%1.%2");
        const auto path = QFileDialog::getSaveFileName(this, tr("Save to"), QString(), pattern.arg("*", AbstractGameModel::saveGameFormat()));
        if (!path.isEmpty()) {
            m_gameModel->saveGame(path);
        }

    });

    connect(m_loadGameBtn, &QPushButton::clicked, [this] () {
        const auto pattern = QStringLiteral("%1.%2");
        const auto path = QFileDialog::getOpenFileName(this, tr("Load game"), QString(), pattern.arg("*", AbstractGameModel::saveGameFormat()));
        if (!path.isEmpty()) {
            m_gameModel->loadGame(path);
        }
    });

    connect(m_clearFieldBtn, &QPushButton::clicked, [this] () {
        m_gameModel->clearField();
    });

    auto rootLayout = new QHBoxLayout(this);
    rootLayout->addWidget(m_gameWidget);
    rootLayout->setStretchFactor(m_gameWidget, 1);

    // init controls
    m_startStopBtn->setCheckable(true);
    m_generateFieldBtn->setText(tr("Generate field"));
    m_saveGameBtn->setText(tr("Save game"));
    m_loadGameBtn->setText(tr("Load game"));
    m_clearFieldBtn->setText(tr("Clear field"));

    auto controlsLayout = new QVBoxLayout;
    controlsLayout->setAlignment(Qt::AlignTop);
    controlsLayout->addWidget(m_startStopBtn);
    controlsLayout->addWidget(m_generateFieldBtn);
    controlsLayout->addWidget(m_saveGameBtn);
    controlsLayout->addWidget(m_loadGameBtn);
    controlsLayout->addWidget(m_clearFieldBtn);
    rootLayout->addLayout(controlsLayout);

    m_onGameState->assignProperty(m_startStopBtn, "text", tr("Stop"));
    m_onGameState->assignProperty(m_generateFieldBtn, "enabled", false);
    m_onGameState->assignProperty(m_saveGameBtn, "enabled", false);
    m_onGameState->assignProperty(m_loadGameBtn, "enabled", false);
    m_onGameState->assignProperty(m_clearFieldBtn, "enabled", false);
    m_onGameState->addTransition(this, &MainWindow::gameStopped, m_outOfGameState);

    m_outOfGameState->assignProperty(m_startStopBtn, "text", tr("Start"));
    m_outOfGameState->assignProperty(m_generateFieldBtn, "enabled", true);
    m_outOfGameState->assignProperty(m_saveGameBtn, "enabled", true);
    m_outOfGameState->assignProperty(m_loadGameBtn, "enabled", true);
    m_outOfGameState->assignProperty(m_clearFieldBtn, "enabled", true);
    m_outOfGameState->addTransition(this, &MainWindow::gameStarted, m_onGameState);

    m_stateMachine->addState(m_onGameState);
    m_stateMachine->addState(m_outOfGameState);
    m_stateMachine->setInitialState(m_outOfGameState);
    m_stateMachine->start();
}
