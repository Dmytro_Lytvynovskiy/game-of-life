#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QWidget>

class QPushButton;
class QStateMachine;
class QState;
class AbstractGameView;
class AbstractGameModel;

class MainWindow : public QWidget
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr) noexcept;
    ~MainWindow() = default;

    QSize sizeHint() const;

signals:
    void gameStarted();
    void gameStopped();

private:
    void initUi() noexcept;

    QStateMachine *m_stateMachine;
    QState *m_onGameState;
    QState *m_outOfGameState;

    QPushButton *m_startStopBtn;
    QPushButton *m_generateFieldBtn;
    QPushButton *m_saveGameBtn;
    QPushButton *m_loadGameBtn;
    QPushButton *m_clearFieldBtn;

    AbstractGameView *m_gameWidget;
    AbstractGameModel *m_gameModel;
};

#endif // MAINWINDOW_H
