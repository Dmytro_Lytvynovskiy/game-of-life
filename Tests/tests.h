#ifndef TESTS_H
#define TESTS_H

#include <QObject>

class Tests : public QObject
{
    Q_OBJECT

public:
    Tests() = default;

private Q_SLOTS:
    void saveLoadTest();
};

#endif // TESTS_H
