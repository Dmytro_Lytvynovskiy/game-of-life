QT += testlib concurrent

TARGET = tst_test
CONFIG += console
CONFIG -= app_bundle

TEMPLATE = app

DEFINES += QT_DEPRECATED_WARNINGS

SOURCES += tests.cpp

HEADERS += tests.h

include($$PWD/../Common/Common.pri)

