#include "tests.h"

#include <QtTest>
#include <QTemporaryFile>

#include "gamemodel.h"

void Tests::saveLoadTest()
{
    AbstractGameModel *model = new GameModel(this);

    // add payload
    model->generateRandomField();
    const QImage originalImg(model->frame());
    QVERIFY2(!originalImg.isNull(), "Initial image is null.");

    QTemporaryFile file;
    const auto opened = file.open();
    QVERIFY2(opened, "Can't create temporary file.");

    // save
    const auto saved = model->saveGame(file.fileName());
    QVERIFY2(saved, "Can't save game field.");

    // reset
    model->clearField();

    // load
    const auto loaded = model->loadGame(file.fileName());
    QVERIFY2(loaded, "Can't load game field.");

    QCOMPARE(model->frame(), originalImg);
}

QTEST_APPLESS_MAIN(Tests)
